## Easy six

Package is a wrapper for [six](https://pypi.python.org/pypi/six) library. 

It helps use autoimport function in IDE properly, thanks to standalone declares of moved modules and attributes.
