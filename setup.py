from setuptools import find_packages, setup

setup(
    name='easy_six',
    version='1.10.4',
    packages=find_packages(),
    url='https://bitbucket.org/p-app/easy_six',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='944950@gmail.com',
    description='Import helpers for six library.',
    install_requires=[
        'six==1.10'
    ]
)
