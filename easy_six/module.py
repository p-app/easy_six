from six import moves

__all__ = [
    'builtins',
    'configparser',
    'copyreg',
    'cPickle',
    '_dummy_thread',
    'email_mime_multipart',
    'email_mime_nonmultipart',
    'email_mime_text',
    'email_mime_base',
    'http_cookiejar',
    'http_cookies',
    'html_entities',
    'html_parser',
    'http_client',
    'BaseHTTPServer',
    'CGIHTTPServer',
    'SimpleHTTPServer',
    'queue',
    'reprlib',
    'socketserver',
    '_thread',
    'urllib',
    'urllib_error',
    'urllib_parse',
    'urllib_robotparser',
    'xmlrpc_client',
    'xmlrpc_server',
]

builtins = moves.builtins
configparser = moves.configparser
# noinspection PyUnresolvedReferences
copyreg = moves.copyreg
cPickle = moves.cPickle
# noinspection PyProtectedMember
_dummy_thread = moves._dummy_thread
email_mime_multipart = moves.email_mime_multipart
email_mime_nonmultipart = moves.email_mime_nonmultipart
email_mime_text = moves.email_mime_text
email_mime_base = moves.email_mime_base
http_cookiejar = moves.http_cookiejar
http_cookies = moves.http_cookies
html_entities = moves.html_entities
html_parser = moves.html_parser
http_client = moves.http_client
BaseHTTPServer = moves.BaseHTTPServer
CGIHTTPServer = moves.CGIHTTPServer
SimpleHTTPServer = moves.SimpleHTTPServer
queue = moves.queue
# noinspection PyUnresolvedReferences
reprlib = moves.reprlib
socketserver = moves.socketserver
# noinspection PyProtectedMember
_thread = moves._thread
urllib = moves.urllib
urllib_robotparser = moves.urllib_robotparser
urllib_parse = moves.urllib_parse
urllib_error = moves.urllib_error
# noinspection PyUnresolvedReferences
xmlrpc_client = moves.xmlrpc_client
# noinspection PyUnresolvedReferences
xmlrpc_server = moves.xmlrpc_server

try:
    winreg = moves.winreg
    __all__.append('winreg')
except AttributeError:
    pass

try:
    # noinspection PyUnresolvedReferences
    dbm_gnu = moves.dbm_gnu
    __all__.append('dbm_gnu')
except ImportError:
    pass

try:
    tkinter = moves.tkinter
    # noinspection PyUnresolvedReferences
    tkinter_dialog = moves.tkinter_dialog
    # noinspection PyUnresolvedReferences
    tkinter_filedialog = moves.tkinter_filedialog
    # noinspection PyUnresolvedReferences
    tkinter_scrolledtext = moves.tkinter_scrolledtext
    # noinspection PyUnresolvedReferences
    tkinter_simpledialog = moves.tkinter_simpledialog
    tkinter_ttk = moves.tkinter_ttk
    # noinspection PyUnresolvedReferences
    tkinter_tix = moves.tkinter_tix
    tkinter_constants = moves.tkinter_constants
    # noinspection PyUnresolvedReferences
    tkinter_dnd = moves.tkinter_dnd
    # noinspection PyUnresolvedReferences
    tkinter_colorchooser = moves.tkinter_colorchooser
    # noinspection PyUnresolvedReferences
    tkinter_commondialog = moves.tkinter_commondialog
    # noinspection PyUnresolvedReferences
    tkinter_tkfiledialog = moves.tkinter_tkfiledialog
    # noinspection PyUnresolvedReferences
    tkinter_font = moves.tkinter_font
    # noinspection PyUnresolvedReferences
    tkinter_messagebox = moves.tkinter_messagebox
    # noinspection PyUnresolvedReferences
    tkinter_tksimpledialog = moves.tkinter_tksimpledialog

    __all__.extend([
        'tkinter',
        'tkinter_dialog',
        'tkinter_filedialog',
        'tkinter_scrolledtext',
        'tkinter_simpledialog',
        'tkinter_ttk',
        'tkinter_tix',
        'tkinter_constants',
        'tkinter_dnd',
        'tkinter_colorchooser',
        'tkinter_commondialog',
        'tkinter_tkfiledialog',
        'tkinter_font',
        'tkinter_messagebox',
        'tkinter_tksimpledialog'
    ])
except ImportError:
    pass
