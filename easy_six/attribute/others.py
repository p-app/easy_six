from six import moves

__all__ = [
    'cStringIO',
    'filter',
    'filterfalse',
    'getcwd',
    'getcwdb',
    'input',
    'intern',
    'map',
    'range',
    'reload_module',
    'reduce',
    'shlex_quote',
    'StringIO',
    'UserDict',
    'UserList',
    'UserString',
    'xrange',
    'zip',
    'zip_longest'
]


cStringIO = moves.cStringIO
# noinspection PyShadowingBuiltins
filter = moves.filter
filterfalse = moves.filterfalse
# noinspection PyShadowingBuiltins
input = moves.input
intern = moves.intern
# noinspection PyShadowingBuiltins
map = moves.map
getcwd = moves.getcwd
getcwdb = moves.getcwdb
# noinspection PyShadowingBuiltins
range = moves.range
reload_module = moves.reload_module
reduce = moves.reduce
shlex_quote = moves.shlex_quote
StringIO = moves.StringIO
UserDict = moves.UserDict
UserList = moves.UserList
UserString = moves.UserString
xrange = moves.xrange
# noinspection PyShadowingBuiltins
zip = moves.zip
zip_longest = moves.zip_longest
