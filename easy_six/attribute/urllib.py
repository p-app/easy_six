from six.moves import urllib_error, urllib_parse, urllib_request, \
    urllib_response, urllib_robotparser

__all__ = [
    'ParseResult',
    'SplitResult',
    'urlparse',
    'urlunparse',
    'parse_qs',
    'parse_qsl',
    'urljoin',
    'urldefrag',
    'urlsplit',
    'urlunsplit',
    'quote',
    'quote_plus',
    'unquote',
    'unquote_plus',
    'urlencode',
    'splittag',
    'splituser',
    'splitquery',
    'uses_fragment',
    'uses_netloc',
    'uses_params',
    'uses_query',
    'uses_relative',
    'ContentTooShortError',
    'URLError',
    'HTTPError',
    'urlopen',
    'install_opener',
    'build_opener',
    'pathname2url',
    'url2pathname',
    'getproxies',
    'Request',
    'OpenerDirector',
    'HTTPDefaultErrorHandler',
    'HTTPRedirectHandler',
    'HTTPCookieProcessor',
    'ProxyHandler',
    'BaseHandler',
    'HTTPPasswordMgr',
    'HTTPPasswordMgrWithDefaultRealm',
    'AbstractBasicAuthHandler',
    'HTTPBasicAuthHandler',
    'ProxyBasicAuthHandler',
    'AbstractDigestAuthHandler',
    'HTTPDigestAuthHandler',
    'ProxyDigestAuthHandler',
    'HTTPHandler',
    'HTTPSHandler',
    'FileHandler',
    'FTPHandler',
    'CacheFTPHandler',
    'UnknownHandler',
    'HTTPErrorProcessor',
    'urlretrieve',
    'urlcleanup',
    'URLopener',
    'FancyURLopener',
    'proxy_bypass',
    'addbase',
    'addclosehook',
    'addinfo',
    'addinfourl',
    'RobotFileParser'
]

# urllib.parse
ParseResult = urllib_parse.ParseResult
SplitResult = urllib_parse.SplitResult
urlparse = urllib_parse.urlparse
urlunparse = urllib_parse.urlunparse
parse_qs = urllib_parse.parse_qs
parse_qsl = urllib_parse.parse_qsl
urljoin = urllib_parse.urljoin
urldefrag = urllib_parse.urldefrag
urlsplit = urllib_parse.urlsplit
urlunsplit = urllib_parse.urlunsplit
quote = urllib_parse.quote
quote_plus = urllib_parse.quote_plus
unquote = urllib_parse.unquote
unquote_plus = urllib_parse.unquote_plus
urlencode = urllib_parse.urlencode
# noinspection PyUnresolvedReferences
splittag = urllib_parse.splittag
# noinspection PyUnresolvedReferences
splituser = urllib_parse.splituser
# noinspection PyUnresolvedReferences
splitquery = urllib_parse.splitquery
# noinspection PyUnresolvedReferences
uses_fragment = urllib_parse.uses_fragment
# noinspection PyUnresolvedReferences
uses_netloc = urllib_parse.uses_netloc
# noinspection PyUnresolvedReferences
uses_params = urllib_parse.uses_params
# noinspection PyUnresolvedReferences
uses_query = urllib_parse.uses_query
# noinspection PyUnresolvedReferences
uses_relative = urllib_parse.uses_relative


# urllib.error
ContentTooShortError = urllib_error.ContentTooShortError
URLError = urllib_error.URLError
HTTPError = urllib_error.HTTPError

# urllib.request
# noinspection PyUnresolvedReferences
urlopen = urllib_request.urlopen
install_opener = urllib_request.install_opener
build_opener = urllib_request.build_opener
# noinspection PyUnresolvedReferences
pathname2url = urllib_request.pathname2url
# noinspection PyUnresolvedReferences
url2pathname = urllib_request.url2pathname
# noinspection PyUnresolvedReferences
getproxies = urllib_request.getproxies
# noinspection PyUnresolvedReferences
Request = urllib_request.Request
OpenerDirector = urllib_request.OpenerDirector
# noinspection PyUnresolvedReferences
HTTPDefaultErrorHandler = urllib_request.HTTPDefaultErrorHandler
HTTPRedirectHandler = urllib_request.HTTPRedirectHandler
# noinspection PyUnresolvedReferences
HTTPCookieProcessor = urllib_request.HTTPCookieProcessor
# noinspection PyUnresolvedReferences
ProxyHandler = urllib_request.ProxyHandler
BaseHandler = urllib_request.BaseHandler
# noinspection PyUnresolvedReferences
HTTPPasswordMgr = urllib_request.HTTPPasswordMgr
# noinspection PyUnresolvedReferences
HTTPPasswordMgrWithDefaultRealm = urllib_request.HTTPPasswordMgrWithDefaultRealm
# noinspection PyUnresolvedReferences
AbstractBasicAuthHandler = urllib_request.AbstractBasicAuthHandler
# noinspection PyUnresolvedReferences
HTTPBasicAuthHandler = urllib_request.HTTPBasicAuthHandler
# noinspection PyUnresolvedReferences
ProxyBasicAuthHandler = urllib_request.ProxyBasicAuthHandler
# noinspection PyUnresolvedReferences
AbstractDigestAuthHandler = urllib_request.AbstractDigestAuthHandler
# noinspection PyUnresolvedReferences
HTTPDigestAuthHandler = urllib_request.HTTPDigestAuthHandler
# noinspection PyUnresolvedReferences
ProxyDigestAuthHandler = urllib_request.ProxyDigestAuthHandler
# noinspection PyUnresolvedReferences
HTTPHandler = urllib_request.HTTPHandler
# noinspection PyUnresolvedReferences
HTTPSHandler = urllib_request.HTTPSHandler
# noinspection PyUnresolvedReferences
FileHandler = urllib_request.FileHandler
FTPHandler = urllib_request.FTPHandler
CacheFTPHandler = urllib_request.CacheFTPHandler
UnknownHandler = urllib_request.UnknownHandler
HTTPErrorProcessor = urllib_request.HTTPErrorProcessor
urlretrieve = urllib_request.urlretrieve
urlcleanup = urllib_request.urlcleanup
URLopener = urllib_request.URLopener
FancyURLopener = urllib_request.FancyURLopener
proxy_bypass = urllib_request.proxy_bypass

# urllib.response
addbase = urllib_response.addbase
addclosehook = urllib_response.addclosehook
addinfo = urllib_response.addinfo
addinfourl = urllib_response.addinfourl

# urllib.robotparser
RobotFileParser = urllib_robotparser.RobotFileParser
